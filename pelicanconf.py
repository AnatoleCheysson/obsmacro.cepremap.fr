#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

LOAD_CONTENT_CACHE = False
DELETE_OUTPUT_DIRECTORY = False

DEFAULT_LANG = 'en'
DEFAULT_DATE_FORMAT = ' %B %-d, %Y'

LOCALE = 'us_US.utf8'

AUTHOR = 'Observatoire Macro'
SITENAME = 'Observatoire Macro'
HIDE_SITENAME = True
SITEURL = 'http://localhost/obsmacro'

DISPLAY_CATEGORY_IN_BREADCRUMBS = True

SHOW_ARTICLE_AUTHOR = True
DISPLAY_ARTICLE_INFO_ON_INDEX = True
DISPLAY_TAGS_INLINE = True

DIRECT_TEMPLATES = ('index', 'tags', 'categories', 'authors', 'archives', 'search')

PATH = 'content'

TIMEZONE = 'Europe/Paris'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
FEED_ALL_RSS = 'feeds/all.rss.xml'
TAG_FEED_RSS = 'feeds/%s.rss.xml'
FEED_USE_SUMMARY = True

# Blogroll
LINKS = (('Dynare', 'http://www.dynare.org/'),
         ('Widukind', 'http://widukind.cepremap.org'),
	 ('R-bloggers', 'http://www.r-bloggers.com'),)

# Social widget
SOCIAL = (('Gitlab', 'https://gitlab.com/ObsMacro'),
          ('Twitter', 'https://twitter.com/obsmacro'),)

DEFAULT_PAGINATION = 5

TWITTER_USERNAME = 'obsmacro'
TWITTER_WIDGET_ID = '573544104640049152'

ARTICLE_URL = 'article/{date:%Y}-{date:%m}/{slug}/'
ARTICLE_SAVE_AS = 'article/{date:%Y}-{date:%m}/{slug}/index.html'
PAGE_URL = '{slug}.html'
PAGE_SAVE_AS = '{slug}.html'

TAGS_URL = 'tags.html'

TAG_CLOUD_MAX_ITEMS = 15

RELATED_POSTS_MAX = 4

HIDE_SIDEBAR = True
DISPLAY_BREADCRUMBS = True
MATH_JAX = {'responsive':'True'}

# Plugins.
PLUGIN_PATHS = ['./plugins']
PLUGINS = ['liquid_tags.img', 'liquid_tags.video', 'feed_summary',
           'render_math','related_posts','simple_footnotes','liquid_tags.youtube',
	   'liquid_tags.notebook','tipue_search']

# Themes
THEME="./theme"

MARKUP = ('md')

PYGMENTS_STYLE = 'trac'
TYPOGRIFY = True

STATIC_PATHS = ['images','notebooks']
SITELOGO = 'images/obsmacro.svg'
SITELOGO_SIZE = 180

SHARIFF = True
SHARIFF_LANG = 'en'
SHARIFF_THEME = 'white'
SHARIFF_SERVICES = '[&quot;twitter&quot;]'
#SHARIFF_BACKEND_URL = 

# License
CC_LICENSE = "CC-BY-SA"

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = False

# Piwik configuration
PIWIK_URL = '46.105.37.67'
PIWIK_SITE_ID= 3
