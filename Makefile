PY?=python
PELICAN?=pelican
PELICANOPTS=

BASEDIR=$(CURDIR)
INPUTDIR=$(BASEDIR)/content
OUTPUTDIR=$(BASEDIR)/output
CONFFILE=$(BASEDIR)/pelicanconf.py
PUBLISHCONF=$(BASEDIR)/publishconf.py

FTP_HOST=localhost
FTP_USER=anonymous
FTP_TARGET_DIR=/

SSH_HOST=quaoar-www
SSH_PORT=22
SSH_USER=obsmacro
SSH_TARGET_DIR=/var/www/obsmacro.cepremap.fr

S3_BUCKET=my_s3_bucket

CLOUDFILES_USERNAME=my_rackspace_username
CLOUDFILES_API_KEY=my_rackspace_api_key
CLOUDFILES_CONTAINER=my_cloudfiles_container

DROPBOX_DIR=~/Dropbox/Public/

GITHUB_PAGES_BRANCH=gh-pages

DEBUG ?= 0
ifeq ($(DEBUG), 1)
	PELICANOPTS += -D
endif

help:
	@echo '                                                                             '
	@echo 'Makefile for a pelican Web site                                              '
	@echo '                                                                             '
	@echo 'Usage:                                                                       '
	@echo '   make html                        (re)generate the web site                '
	@echo '   make clean                       remove the generated files               '
	@echo '   make regenerate                  regenerate files upon modification       '
	@echo '   make publish                     generate using production settings       '
	@echo '   make push                        push local commits on gitlab.com/ObsMacro'
	@echo '   make upload                      upload the web site via rsync+ssh        '
	@echo '                                                                             '
	@echo 'Set the DEBUG variable to 1 to enable debugging, e.g. make DEBUG=1 html      '
	@echo '                                                                             '

crises-ue-eu.md:
	wget https://gitlab.com/ObsMacro/crises-ue-eu/raw/master/crises-ue-eu.md
	mv crises-ue-eu.md content/crises-ue-eu.md
	sed -i 's/\.\/img/http\:\/\/obsmacro\.cepremap\.fr\/figures\/crises\-ue\-eu/g' content/crises-ue-eu.md

get-posts: crises-ue-eu.md

html: get-posts
	$(PELICAN) $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)

clean:
	[ ! -d $(OUTPUTDIR) ] || rm -rf $(OUTPUTDIR)
	rm content/crises-ue-eu.md

regenerate: get-posts
	$(PELICAN) -r $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)

publish: get-posts
	$(PELICAN) $(INPUTDIR) -o $(OUTPUTDIR) -s $(PUBLISHCONF) $(PELICANOPTS)

push:
	git push gitlab master

upload: publish push
	rsync -e "ssh -p $(SSH_PORT)" -P -ravzc $(OUTPUTDIR)/ $(SSH_USER)@$(SSH_HOST):$(SSH_TARGET_DIR) --cvs-exclude

.PHONY: html help clean regenerate publish upload
