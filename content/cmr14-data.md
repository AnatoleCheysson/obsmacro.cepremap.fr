Title: Automating update of the Christiano, Motto and Rostagno (2014) database for the United-States
Date: 2016-06-02
Category: Data
Tags: database, DSGE, bayesian estimation, R
Slug: cmr14-data
Authors: Thomas Brand, Anatole Cheysson
Summary: Description step by step to build automatic update of the Christiano, Motto and Rostagno (2014) database for the United States.
Download: https://gitlab.com/ObsMacro/cmr14-data
ToggleSource: True

<div class="large"><iframe id="iFrameResizer0"  src="http://shiny.obsmacro.org/cmr14-data/" width='100%'></iframe></div>

