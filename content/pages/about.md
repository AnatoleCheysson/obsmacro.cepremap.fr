Title: About
Slug: about

The CEPREMAP hosts the Macroeconomic Observatory, a research team dedicated to macroeconomic modelling directed by Jean-Olivier Hairault, Michel Juillard and François Langot. The team is supervised by Thomas Brand, its goal is to:

* help central banks and other policy institutions to develop their own macroeconomic models based on the latest theoretical and empirical developments;

* advance the research in the field of computational techniques devoted to the resolution and estimation of these macroeconomic models;

* conduct analysis and forecast of French, European and world economies.

In particular, the CEPREMAP modelling team leads the development of Dynare, a software for handling a wide class of economic models, which has received wide acceptance within both academic and policy circles. The Dynare development is supervised by Stéphane Adjemian. 

The team has also developed long-term relationships with policy institutions, in particular the International Monetary Fund, the Banque de France, the European Commission and the Cabinet Office of the Prime Minister of Japan. The central bank network using forecast of the GPM model is coordinated by Ferhat Mihoubi.

The team always welcomes candidacy for research assistant. If you are interested in our work, please follow this [link](http://obsmacro.cepremap.fr/internship.html).

</div>
<div class="team-container">
<h2>Our Team</h2>
<div class="row team-list">
	<div class="team-list-item col-md-3 col-sm-6">
		<img src="images/id_photos/SA.jpg" alt="Stéphane Adjemian">
		<h3 class="team-list-title">Stéphane Adjemian</h3>
		<a href="http://www.dynare.org/stepan/" target="_blank"><i class="fa fa-external-link"></i> Website</a>
	</div>
	<div class="team-list-item col-md-3 col-sm-6">
		<img src="images/id_photos/NA.jpg" alt="Nesma Ali">
		<h3 class="team-list-title">Nesma Ali</h3>
		<a href="#" target="_blank"><i class="fa fa-external-link"></i> Website</a>
	</div>
	<div class="team-list-item col-md-3 col-sm-6">
		<img src="images/id_photos/HB.jpg" alt="Houtan Bastani">
		<h3 class="team-list-title">Houtan Bastani</h3>
		<a href="http://www.dynare.org/houtan/" target="_blank"><i class="fa fa-external-link"></i> Website</a>
	</div>
	<div class="team-list-item col-md-3 col-sm-6">
		<img src="images/id_photos/TB.jpg" alt="Thomas Brand">
		<h3 class="team-list-title">Thomas Brand</h3>
		<a href="http://obsmacro.cepremap.fr/about-TB.html" target="_blank"><i class="fa fa-external-link"></i> Website</a>
	</div>
	<div class="team-list-item col-md-3 col-sm-6">
		<img src="images/id_photos/AC.jpg" alt="Anatole Cheysson">
		<h3 class="team-list-title">Anatole Cheysson</h3>
		<a href="#" target="_blank"><i class="fa fa-external-link"></i> Website</a>
	</div>
	<div class="team-list-item col-md-3 col-sm-6">
		<img src="images/id_photos/AE.jpg" alt="Aygul Evdokimova">
		<h3 class="team-list-title">Aygul Evdokimova</h3>
		<a href="" target="_blank"><i class="fa fa-external-link"></i> Website</a>
	</div>
	<div class="team-list-item col-md-3 col-sm-6">
		<img src="images/id_photos/JOH.jpg" alt="Jean-Olivier Hairault">
		<h3 class="team-list-title">Jean-Olivier Hairault</h3>
		<a href="http://ces.univ-paris1.fr/membre/Hairault/hairaultang.htm" target="_blank"><i class="fa fa-external-link"></i> Website</a>
	</div>
	<div class="team-list-item col-md-3 col-sm-6">
		<img src="images/id_photos/EI.jpg" alt="Eleni Iliopulos">
		<h3 class="team-list-title">Eleni Iliopulos</h3>
		<a href="http://eleni.iliopulos.free.fr/" target="_blank"><i class="fa fa-external-link"></i> Website</a>
	</div>
	<div class="team-list-item col-md-3 col-sm-6">
		<img src="images/id_photos/MJ.png" alt="Michel Juillard">
		<h3 class="team-list-title">Michel Juillard</h3>
		<a href="http://www.mjui.fr/juillard/" target="_blank"><i class="fa fa-external-link"></i> Website</a>
	</div>
	<div class="team-list-item col-md-3 col-sm-6">
		<img src="images/id_photos/SK.jpg" alt="Sumudu Kankanamge">
		<h3 class="team-list-title">Sumudu Kankanamge</h3>
		<a href="http://kankanamge.free.fr/" target="_blank"><i class="fa fa-external-link"></i> Website</a>
	</div>
	<div class="team-list-item col-md-3 col-sm-6">
		<img src="images/id_photos/FK.jpg" alt="Frédéric Karamé">
		<h3 class="team-list-title">Frédéric Karamé</h3>
		<a href="http://f.karame.free.fr/research.php" target="_blank"><i class="fa fa-external-link"></i> Website</a>
	</div>
	<div class="team-list-item col-md-3 col-sm-6">
		<img src="images/id_photos/FL.png" alt="François Langot">
		<h3 class="team-list-title">François Langot</h3>
		<a href="http://perso.univ-lemans.fr/~flangot/" target="_blank"><i class="fa fa-external-link"></i> Website</a>
	</div>
	<div class="team-list-item col-md-3 col-sm-6">
		<img src="images/id_photos/AM.jpg" alt="Antoine Mayerowitz">
		<h3 class="team-list-title">Antoine Mayerowitz</h3>
		<a href="#" target="_blank"><i class="fa fa-external-link"></i> Website</a>
	</div>
	<div class="team-list-item col-md-3 col-sm-6">
		<img src="images/id_photos/FM.jpg" alt="Ferhat Mihoubi">
		<h3 class="team-list-title">Ferhat Mihoubi</h3>
		<a href="http://www.erudite.univ-paris-est.fr/lequipe/enseignants-chercheurs/mihoubi-ferhat/" target="_blank"><i class="fa fa-external-link"></i> Website</a>
	</div>
	<div class="team-list-item col-md-3 col-sm-6">
		<img src="images/id_photos/GN.jpg" alt="Guillaume Névo">
		<h3 class="team-list-title">Guillaume Névo</h3>
		<a href="#" target="_blank"><i class="fa fa-external-link"></i> Website</a>
	</div>
	<div class="team-list-item col-md-3 col-sm-6">
		<img src="images/id_photos/EP.jpg" alt="Erica Perego">
		<h3 class="team-list-title">Erica Perego</h3>
		<a href="https://sites.google.com/site/ericaritaperego/home" target="_blank"><i class="fa fa-external-link"></i> Website</a>
	</div>
	<div class="team-list-item col-md-3 col-sm-6">
		<img src="images/id_photos/AR.jpg" alt="Antoine Roy">
		<h3 class="team-list-title">Antoine Roy</h3>
		<a href="#" target="_blank"><i class="fa fa-external-link"></i> Website</a>
	</div>
	<div class="team-list-item col-md-3 col-sm-6">
		<img src="images/id_photos/TS.jpg" alt="Thepthida Sopraseuth">
		<h3 class="team-list-title">Thepthida Sopraseuth</h3>
		<a href="http://thepthida.sopraseuth.free.fr/" target="_blank"><i class="fa fa-external-link"></i> Website</a>
	</div>
	<div class="team-list-item col-md-3 col-sm-6">
		<img src="images/id_photos/TW.jpg" alt="Thomas Weitzenblum">
		<h3 class="team-list-title">Thomas Weitzenblum</h3>
		<a href="http://weitzenblum.free.fr/" target="_blank"><i class="fa fa-external-link"></i> Website</a>
	</div>
	<div class="team-list-item col-md-3 col-sm-6">
		<img src="images/id_photos/TZ.jpg" alt="Thomas Zuber">
		<h3 class="team-list-title">Thomas Zuber</h3>
		<a href="#" target="_blank"><i class="fa fa-external-link"></i> Website</a>
	</div>
</div>


