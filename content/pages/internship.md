Title: Internship
Status: hidden
Slug: internship

The Macroeconomic Observatory at CEPREMAP offers research assistant positions (at least 3 months) in quantitative macroeconomics. Research assistant will work either on academic programs or more applied projects in relation with international institutions.

The research assistant will learn how to :

* produce macroeconomic database
* make data visualisation
* implement statistics toolbox for economic monitoring
* solve and simulate DSGE models
* estimate DSGE models
* forecast key variables for various countries/regions

Students with a master or in PhD with quantitative skills (econometrics, programming in R, matlab, python and dynare) can email their resume to obsmacro@cepremap.fr.
