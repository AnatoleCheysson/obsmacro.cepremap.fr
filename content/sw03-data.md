Title: Automating update of the Smets and Wouters (2003) database
Date: 2015-10-15
Category: Data
Tags: database, DSGE, bayesian estimation, R
Slug: sw03-data
Authors: Thomas Brand, Nicolas Toulemonde
Summary: Description step by step to build automatic update of the Smets and Wouters (2003) database.
Download: https://gitlab.com/ObsMacro/sw03-data
ToggleSource: True

<div class="large"><iframe id="iFrameResizer0"  src="http://shiny.obsmacro.org/sw03-data/" width='100%'></iframe></div>

