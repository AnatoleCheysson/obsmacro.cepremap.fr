Title: Automating update of the Christiano, Motto and Rostagno (2014) database for the Euro Area
Date: 2016-06-01
Category: Data
Tags: database, DSGE, bayesian estimation, R
Slug: cmr14-EA-data
Authors: Thomas Brand
Summary: Description step by step to build automatic update of the Christiano, Motto and Rostagno (2014) database for the Euro Area.
Download: https://gitlab.com/ObsMacro/cmr14-EA-data
ToggleSource: True

<div class="large"><iframe id="iFrameResizer0" src="http://shiny.obsmacro.org/cmr14-EA-data/" width='100%'></iframe></div>

