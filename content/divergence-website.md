Title: Financial factors in the US and Euro Area business cycles: a general equilibrium approach
Date: 2016-07-01
Category: Policy
Tags: model, DSGE, bayesian estimation, R
Slug: divergence-website
Authors: Thomas Brand, Fabien Tripier
Summary: Comparison of different estimations of the Christiano, Motto, Rostagno (2014) model, using Dynare and updated databases for the [United States]({filename}cmr14-data.md) and the [Euro area]({filename}cmr14-EA-data.md).
Download: https://gitlab.com/BrandTripier/divergence-website
ToggleSource: True

<div class="large"><iframe id="iFrameResizer0"  src="http://shiny.obsmacro.org/divergence-website/" width='100%'></iframe></div>

